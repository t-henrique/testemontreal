﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio10
{
    public class Ponto
    {
        protected float x { get; set; }
        protected float y { get; set; }

        public Ponto (float a, float b)
        {
            x = a;
            y = b;
        }
    }
}
