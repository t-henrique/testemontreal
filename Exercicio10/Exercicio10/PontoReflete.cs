﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio10
{
    public class PontoReflete : Ponto
    {
        public PontoReflete(float a, float b) : base(a, b)
        {
        }

        public void reflete()
        {
            var inverter = x;
            x = y;
            y = inverter;

            Console.WriteLine($"As variáveis foram invertidas, o novo valor de X é: {x} e o novo valor de Y é: {y}");
        }

    }
}
