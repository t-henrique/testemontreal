﻿using System;

namespace Exercicio10
{
    class Program
    {
        static void Main(string[] args)
        {
            float a, b;

            Console.WriteLine("Insira um número inteiro para a variável A:");
            a = float.Parse(Console.ReadLine());

            Console.WriteLine("Insira outro número inteiro B:");
            b = float.Parse(Console.ReadLine());
            
            Console.WriteLine("Veja o resultado das operações:");

            //var ponto = new Ponto(a, b);
            var pMove = new PontoMove(a,b);
            pMove.move();

            var pReflete = new PontoReflete(a, b);
            pReflete.reflete();

            Console.ReadKey();


        }
    }
}
