﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio10
{
    public class PontoMove : Ponto
    {
        

        public PontoMove(float a, float b) : base(a, b)
        {
        }

        public void move()
        {
            x = x + x;
            y = y + y;

            Console.WriteLine($"As variáveis foram movidas, o novo valor de X é: {x} e o novo valor de Y é: {y}");
        }
    }
}
