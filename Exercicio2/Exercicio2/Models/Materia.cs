﻿using Exercicio2.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Exercicio2.Models
{
    public class Materia
    {
        public static List<Materia> materias = new Materia().retornaMaterias();
        public int Codigo { get; set; }
        public string Nome { get; set; }

        public List<Materia> retornaMaterias()
        {
            Xml dao = new Xml();

            DataSet dsMaterias = dao.retornaDadosEmXml();
            List<Materia> materia = new List<Materia>();
            materia = (from rows in dsMaterias.Tables[0].AsEnumerable()
                       select new Materia
                       {
                           Nome = rows[0].ToString(),
                           Codigo = Convert.ToInt32(rows[1].ToString())
                       }).ToList();

            return materia;
        }

        public void adicionaNovaMateria(Materia materia)
        {
            materias.Add(new Materia()
            {
                Codigo = materias.OrderByDescending(o => o.Codigo).Select(x => x.Codigo).FirstOrDefault() + 1,
                Nome = materia.Nome
            });
        }
    }


}