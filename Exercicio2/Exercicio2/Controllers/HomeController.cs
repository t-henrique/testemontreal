﻿using Exercicio2.Models;
using System.Web.Mvc;

namespace Exercicio2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        { 
            ViewBag.Materias = new SelectList
                (
                    Materia.materias,
                    "Codigo",
                    "Nome"
                );

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}