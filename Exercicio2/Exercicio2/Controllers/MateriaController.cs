﻿using Exercicio2.Models;
using System.Web.Mvc;

namespace Exercicio2.Controllers
{
    public class MateriaController : Controller
    {
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Materia materia)
        {
            if (ModelState.IsValid)
            {
                var novaMateria = new Materia();

                novaMateria.adicionaNovaMateria(materia);
            }
            return RedirectToAction("Create");
        }
    }
}