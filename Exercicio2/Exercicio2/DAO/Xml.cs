﻿using System.Data;
using System.Xml;
using System.Xml.Linq;

namespace Exercicio2.DAO
{
    public class Xml
    {
        private XmlReader carregaXmlMateriasLocal()
        {
            XElement xmlData = XElement.Load(@"C:\montreal\testemontreal\Exercicio2\Exercicio2\xmlExemplo\exemplo.xml");

            return xmlData.CreateReader();
        }

        public DataSet retornaDadosEmXml()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(carregaXmlMateriasLocal());

            return ds;
        }

    }
}